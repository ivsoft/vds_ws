import {
    decode64,
    decrypt_by_private_key,
    decrypt_by_aes_256_cbc,
    buffer_pop_data,
    base64,
    public_key_from_Der,
    private_key_from_Der,
    public_key_fingerprint,
    buffer_from_string_raw,
    create_channel_message
} from './vds_crypto';

import {user_message_transaction, file_info} from './transactions/user_message_transaction';


class vds_channel {
    constructor(channel_id, channel_type, name, read_public_key, read_private_key, write_public_key, write_private_key)
    {
        this.channel_id = channel_id;
        this.channel_type = channel_type;
        this.channel_name = name;

        this.read_public_key = public_key_from_Der(read_public_key);
        this.read_key_id = base64(public_key_fingerprint(this.read_public_key));

        this.read_private_key = private_key_from_Der(read_private_key);

        this.write_public_key = public_key_from_Der(write_public_key);
        this.write_key_id = base64(public_key_fingerprint(this.write_public_key));

        this.write_private_key = private_key_from_Der(write_private_key);

        this.read_keys_ = new Map();
        this.write_keys_ = new Map();

        this.add_read_key(
            this.read_key_id,
            { public_key: this.read_public_key, private_key: this.read_private_key });
        this.add_write_key(
            this.write_key_id,
            { public_key: this.write_public_key, private_key: this.write_private_key });

        this.messages = [];
        this.files = new Map();
    }

    add_read_key(id, key){
        this.read_keys_.set(id, key);
    }

    add_write_key(id, key){
        this.write_keys_.set(id, key);
    }


    decrypt(message) {
        const read_keys = this.read_keys_.get(message.read_id);
        const key_data = decrypt_by_private_key(read_keys.private_key, decode64(message.crypted_key));

        const key_data_buffer = buffer_from_string_raw(key_data, 'raw');
        const key = buffer_pop_data(key_data_buffer);
        const iv = buffer_pop_data(key_data_buffer);
    
        const data = decrypt_by_aes_256_cbc(key, iv, decode64(message.crypted_data));

        const message_id = data.getByte();
        switch(message_id){
            //user_message_transaction
            case 97:{
                const body = user_message_transaction.deserialize(data);
                return {
                    type: 'user_message_transaction',
                    body: body
                };
            }
        }

        throw "Invalid message " + message_id;
    }

    crypt(message) {
        if(!this.write_key_id){
            throw 'Channel is read only';
        }

        const read_keys = this.read_keys_.get(this.read_key_id);
        const write_keys = this.write_keys_.get(this.write_key_id);

        return create_channel_message(
            decode64(this.channel_id),
            decode64(this.read_key_id),
            decode64(this.write_key_id),
            message,
            read_keys.public_key,
            write_keys.private_key);
    }

    serialize_message(message, files){
        console.log(files);
        let transaction = new user_message_transaction(
            message,
            files.map(x => new file_info(x.file_name, x.mime_type, x.file_size, x.file_id, x.file_blocks))
        );

        return transaction.serialize();
    }
}

export default vds_channel;