import { 
    decode64,
    decrypt_by_private_key,
    decrypt_by_aes_256_cbc,
    buffer_pop_data,
    buffer_get_string,
    base64,
    hash_sha256,
    crypt_by_aes_256_cbc,
    create_buffer,
    from_hex,
    buffer_from_string_raw,
    Uint8Array_to_raw,
    Uint8Array_from_raw,
    create_channel_message,
    public_key_fingerprint
} from './vds_crypto';
import pako from 'pako';
import vds_channel from './vds_channel';
import {file_block} from './transactions/user_message_transaction';
import { create_user_transaction } from './transactions/create_user_transaction';
import { channel_create_transaction } from './transactions/channel_create_transaction';

class vds_ws {

    constructor() {
        this.callback_ = new Map();
        this.subscription_ = new Map();
        this.read_keys_ = new Map();
        this.write_keys_ = new Map();

        this.id_ = 0;
        this.is_opened_ = false;
    }

    async connect() {
        this.ws_ = new WebSocket('ws://' + window.location.hostname + ':8050/api/ws');

        var promise = new Promise((resolve, reject) => {
            this.callback_.set(0, { resolve, reject });
        });

        this.ws_.onopen = event => {
            const handler = this.callback_.get(0);
            this.callback_.delete(0);
            handler.resolve();
        };

        this.ws_.onclose = event => {
            this.callback_.forEach(function (item) {
                item.reject("Соединение прервано");
            });
            this.callback_.clear();
            this.ws_ = null;
        };

        this.ws_.onmessage = event => {
            const msg = JSON.parse(event.data);
            const id = +msg.id;
            const handler = this.callback_.get(id);
            if(handler){
                this.callback_.delete(id);
                if(msg.result){
                    handler.resolve(msg.result);
                }
                else {
                    handler.reject(msg.error);
                }
            }
            else {
                const subscription = this.subscription_.get(id);
                if(subscription){
                    subscription(msg.result);
                }
            }
        };

        this.ws_.onerror = event => {
            this.callback_.forEach(function (item) {
                item.reject("Соединение прервано");
            });
            this.callback_.clear();
        };

        return await promise;
    }

    async invoke(method, message) {
        if (null == this.ws_) {
            await this.connect();
        }

        this.id_++;
        const id = this.id_;
        let promise = new Promise((resolve, reject) => {
            this.callback_.set(id, { resolve, reject });
        });

        this.ws_.send(JSON.stringify({
            id: id,
            invoke: method,
            params: message
        }));

        return await promise;
    }

    async subscribe(method, params, callback){
        if (null == this.ws_) {
            await this.connect();
        }

        this.id_++;
        const id = this.id_;
        this.subscription_.set(id, callback);

        return await this.invoke("subscribe", [ id, method, params ]);
    }

    add_read_key(id, key){
        this.read_key_id_ = id;
        this.read_keys_.set(id, key);
    }

    add_write_key(id, key){
        this.write_key_id_ = id;
        this.write_keys_.set(id, key);
    }

    decrypt(message) {
        const read_keys = this.read_keys_.get(message.read_id);
        const key_data = decrypt_by_private_key(read_keys.private_key, decode64(message.crypted_key));

        const key_data_buffer = buffer_from_string_raw(key_data, 'raw');
        const key = buffer_pop_data(key_data_buffer);
        const iv = buffer_pop_data(key_data_buffer);

        const data = decrypt_by_aes_256_cbc(key, iv, decode64(message.crypted_data));

        const message_id = data.getByte();
        switch(message_id){
            //channel_create_transaction
            case 110:{
                const channel_data = channel_create_transaction.deserialize(data);

                if(0 != data.length()){
                    throw "Invalid message " + channel_data.channel_name;
                }

                return {
                    type: 'channel_create',
                    channel: new vds_channel(
                        base64(channel_data.channel_id),
                        channel_data.channel_type,
                        channel_data.channel_name,
                        channel_data.read_public_key,
                        channel_data.read_private_key,
                        channel_data.write_public_key,
                        channel_data.write_private_key)
                };
            }

            //channel_message
            case 99: {
                const channel_id = base64(buffer_pop_data(data));
                const read_id = base64(buffer_pop_data(data));
                const write_id = base64(buffer_pop_data(data));
                const crypted_key = base64(buffer_pop_data(data));
                const crypted_data = base64(buffer_pop_data(data));
                const signature = base64(buffer_pop_data(data));

                return {
                    type: 'channel_message',
                    channel_id: channel_id,
                    read_id: read_id,
                    write_id: write_id,
                    crypted_key: crypted_key,
                    crypted_data: crypted_data,
                    signature: signature
                };
            }
        }

        throw "Invalid message " + message_id;
    }

    async save_block(data){
        const buffer = create_buffer(data);
        const key_data = hash_sha256(buffer.data);
        const iv_data = from_hex('a5bb9fcec2e44b91a8c9594462559024');

        const key_data2 = hash_sha256(crypt_by_aes_256_cbc(key_data, iv_data, buffer.data));
        const zipped = pako.deflate(Uint8Array_from_raw(buffer.data));
        const crypted_data = crypt_by_aes_256_cbc(key_data2, iv_data, zipped);
        const result = await this.invoke('upload', [base64(crypted_data)]);
        return new file_block(base64(key_data), key_data2, result.replicas, data.byteLength);
    }

    async download(file_block){
        const result = await this.invoke('download', file_block.replica_hashes);
        return this.decrypt_file_block(file_block, decode64(result));
    }

    decrypt_file_block(file_block, result) {
        const iv_data = from_hex('a5bb9fcec2e44b91a8c9594462559024');
        const zipped = decrypt_by_aes_256_cbc(file_block.block_key, iv_data, result);
        const original_data = pako.inflate(zipped.data);
        const sig = base64(hash_sha256(Uint8Array_to_raw(original_data)));
        if(file_block.block_id != sig) {
            throw "Data is corrupted";
        }

        return original_data;
    }

    async register_user(user_name, user_password){
        await this.invoke(
            'broadcast', 
            [base64(create_user_transaction.new_user(user_name, user_password).serialize())]);
    }

    async channel_create(channel_type, channel_name){
        const channel_data = await channel_create_transaction.channel_create(channel_type, channel_name);

        const read_key = this.read_keys_.get(this.read_key_id_);
        if(!read_key){
            throw new 'Unable to get read key';
        }
        const write_key = this.write_keys_.get(this.write_key_id_);
        if(!write_key){
            throw new 'Unable to get write key';
        }
        const message = create_channel_message(
            public_key_fingerprint(write_key.public_key),
            decode64(this.read_key_id_),
            decode64(this.write_key_id_),
            channel_data.serialize(),
            read_key.public_key,
            write_key.private_key);

        await this.invoke(
            'broadcast',
            [base64(message)]);

        return base64(channel_data.channel_id);
    }
}

export default vds_ws;