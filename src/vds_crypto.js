import createHash from "create-hash";
//import CryptoJS from 'crypto-js';
import forge from 'node-forge';

/** @arg {string|Buffer} data
    @arg {string} [digest = null] - 'hex', 'binary' or 'base64'
    @return {string|Buffer} - Buffer when digest is null, or string
*/
function sha256(data, encoding) {
    return createHash("sha256")
        .update(data)
        .digest(encoding);
}


export function  user_credentials_to_key(user_password) {
    const password_hash = sha256(user_password, 'base64');
    return password_hash.length + "." + password_hash;
}

export function symmetric_key_from_password(user_password){
    const result = forge.pkcs5.pbkdf2(user_password, forge.util.hexToBytes('dd04ee6a8ad646719b4f9afbc7f673f8'), 1000, 32, forge.md.sha512.create());
    return result;
}

export function decrypt_private_key(private_key, user_password){
    const key = symmetric_key_from_password(user_password);

    var decipher = forge.cipher.createDecipher('AES-CBC', key);
    decipher.start({iv: forge.util.hexToBytes('00000000000000000000000000000000')});
    const pk = forge.util.decode64(private_key);
    decipher.update(forge.util.createBuffer(pk, 'raw'));
    if(!decipher.finish()){
        throw "Decrypt error";
    }

    const asn = forge.asn1.fromDer(decipher.output);
    const result = forge.pki.privateKeyFromAsn1(asn);

    return result;
}

export function crypt_private_key(private_key, user_password){
    const key = symmetric_key_from_password(user_password);

    var cipher = forge.cipher.createCipher('AES-CBC', key);
    cipher.start({iv: forge.util.hexToBytes('00000000000000000000000000000000')});

    const asn = forge.pki.privateKeyToAsn1(private_key);
    const pk = forge.asn1.toDer(asn);

    cipher.update(forge.util.createBuffer(pk, 'raw'));
    if(!cipher.finish()){
        throw "Crypt error";
    }

    return cipher.output.getBytes();
}

export function private_key_from_Der(private_key){
    const asn = forge.asn1.fromDer(private_key);
    const result = forge.pki.privateKeyFromAsn1(asn);

    return result;
}

export function private_key_to_Der(private_key){
    const asn = forge.pki.privateKeyToAsn1(private_key);
    const result = forge.asn1.toDer(asn);

    return result.getBytes();
}

export function public_key_from_Der(private_key){
    const asn = forge.asn1.fromDer(private_key);
    const result = forge.pki.publicKeyFromAsn1(asn);

    return result;
}

export function public_key_to_Der(public_key){
    const asn = forge.pki.publicKeyToRSAPublicKey(public_key);
    const result = forge.asn1.toDer(asn);

    return result.getBytes();
}

export function parse_public_key(public_key){
    const result = forge.pki.publicKeyFromPem(public_key);
    return result;
}

export function public_key_fingerprint(public_key){
    const result = forge.ssh.getPublicKeyFingerprint(public_key, { md: forge.md.sha256.create()});
    return result.getBytes();

}

export function hash_sha256(data){
    var md = forge.md.sha256.create();
    md.start();
    md.update(data, 'raw');
    const result = md.digest();
    return result.getBytes();
}

export function hash_sha256_begin(){
    var md = forge.md.sha256.create();
    md.start();
    return md;
}

export function hash_sha256_update(md, data){
    md.update(data, 'raw');
}

export function hash_sha256_finish(md){
    const result = md.digest();
    return result.getBytes();
}


export function base64(data){
    const result = forge.util.encode64(data);
    return result;
}

export function from_hex(data){
    return forge.util.hexToBytes(data);
}

export function create_buffer(data){
    if(data){
        return forge.util.createBuffer(data, 'raw');
    }
    else {
        return forge.util.createBuffer();
    }
}

export function decode64(data){
    const result = forge.util.decode64(data);
    return result;
}

export function decrypt_by_private_key(private_key, message){
    var result = private_key.decrypt(message);
    return result;
}

export function buffer_get_int64(buffer)
{
    let result = 0;
    for(let i = 0; i < 8; ++i){
        const value = buffer.getByte();
        result <<= 8;
        result |= value;
    }

    return result;
}

export function buffer_push_int64(buffer, value)
{
    buffer.putByte((value >> 56) & 0xFF);
    buffer.putByte((value >> 48) & 0xFF);
    buffer.putByte((value >> 40) & 0xFF);
    buffer.putByte((value >> 32) & 0xFF);
    buffer.putByte((value >> 24) & 0xFF);
    buffer.putByte((value >> 16) & 0xFF);
    buffer.putByte((value >> 8) & 0xFF);
    buffer.putByte(value & 0xFF);
}

export function buffer_read_number(buffer) {
    let value = buffer.getByte();
    
    if(0x80 > value){
      return value;
    }
    
    let result = 0;
    for(let i = (value & 0x7F); i > 0; --i){
        value = buffer.getByte();
        result <<= 8;
        result |= value;
    }
    
    return result + 0x80;  
}

export function buffer_write_number(buffer, value){
  // 0 .... 7 bit
  if (128 > value) {
    buffer.putByte(value);
    return;
  }
  
  value -= 128;
  
  let data = [];
  let index = 0;
  do {
    data.push(value & 0xFF);
    ++index;
    value >>= 8;
  } while (0 != value);
  
  buffer.putByte(0x80 | index);
  while(index > 0){
    buffer.putByte(data[--index]);
  }
}

export function buffer_pop_data(buffer) {
    const len = buffer_read_number(buffer);
    return buffer.getBytes(len);
}

export function buffer_get_string(buffer) {
    const data = buffer_pop_data(buffer);
    return forge.util.decodeUtf8(data);
}

export function buffer_push_string(buffer, str) {
    const data = forge.util.encodeUtf8(str);
    buffer_push_data(buffer, data, true);
}

export function buffer_from_string_raw(data) {
    return forge.util.createBuffer(data, 'raw');
}

export function Uint8Array_to_raw(data) {
    return forge.util.binary.raw.encode(data);
}

export function Uint8Array_from_raw(data) {
    return forge.util.binary.raw.decode(data);
}


export function buffer_push_data(buffer, data, serialize_size) {
    if(serialize_size){
        buffer_write_number(buffer, data.length);
    }
    buffer.putBytes(data);
}


export function decrypt_by_aes_256_cbc(key, iv, message){
    const decipher = forge.cipher.createDecipher('AES-CBC', key);
    decipher.start({iv:iv});
    decipher.update(forge.util.createBuffer(message, 'raw'));
    if(!decipher.finish()){
        throw "Decrypt error";
    }

    return decipher.output;
}



export function crypt_by_aes_256_cbc(key, iv, message){
    const cipher = forge.cipher.createCipher('AES-CBC', key);
    cipher.start({iv:iv});
    cipher.update(forge.util.createBuffer(message, 'raw'));
    if(!cipher.finish()){
        throw "Crypt error";
    }

    return cipher.output.data;
}

export function create_channel_message(channel_id, read_id, write_id, message, write_public_key, write_private_key)
{
    var key = forge.random.getBytesSync(32);
    var iv = forge.random.getBytesSync(16);
    const crypted_data = crypt_by_aes_256_cbc(key, iv, message);

    var key_data_buffer = new forge.util.ByteStringBuffer();
    buffer_push_data(key_data_buffer, key, true);
    buffer_push_data(key_data_buffer, iv, true);

    const crypted_key = write_public_key.encrypt(key_data_buffer.data);

    var buffer = new forge.util.ByteStringBuffer();
    buffer.putByte(99);
    buffer_push_data(buffer, channel_id, true);
    buffer_push_data(buffer, read_id, true);
    buffer_push_data(buffer, write_id, true);
    buffer_push_data(buffer, crypted_key, true);
    buffer_push_data(buffer, crypted_data, true);


    let md = forge.md.sha256.create();
    md.start();
    md.update(buffer.data, 'raw');

    var signature = write_private_key.sign(md);
    buffer_push_data(buffer, signature, true);

    return buffer.data;
}

export function rsa_generateKeyPair(bits){
    return new Promise(resolve => {
        resolve(forge.pki.rsa.generateKeyPair(bits));
    });
}
