import {
    buffer_pop_data,
    buffer_push_data,
    buffer_get_string,
    buffer_push_string,
    buffer_read_number,
    buffer_write_number,
    buffer_get_int64,
    buffer_push_int64,
    base64,
    decode64,
    create_buffer
} from '../vds_crypto';

export class file_block {
    constructor(block_id, block_key, replica_hashes, block_size)
    {
        this.block_id = block_id;
        this.block_key = block_key;
        this.replica_hashes = replica_hashes;
        this.block_size = block_size;
    }

    serialize(buffer) {
        buffer_push_data(buffer, decode64(this.block_id), true);
        buffer_push_data(buffer, this.block_key, true);

        buffer_write_number(buffer, this.replica_hashes.length);
        for(var i = 0; i < this.replica_hashes.length; ++i){
            buffer_push_data(buffer, decode64(this.replica_hashes[i]), true);
        }
        buffer_push_int64(buffer, this.block_size);
    }

    static deserialize(data) {
        const block_id = base64(buffer_pop_data(data));
        const block_key = buffer_pop_data(data);

        const row_count = buffer_read_number(data);
        var replica_hashes = [];
        for(var i = 0; i < row_count; ++i){
            replica_hashes.push(base64(buffer_pop_data(data)));
        }
        const block_size = buffer_get_int64(data);

        return new file_block(block_id, block_key, replica_hashes, block_size);
    }
}

export class file_info {
    constructor(name, mime_type, size, file_id, file_blocks)
    {
        this.name = name;
        this.mime_type = mime_type;
        this.size = size;
        this.file_id = file_id;
        this.file_blocks = file_blocks;

    }

    serialize(buffer) {
        buffer_push_string(buffer, this.name);
        buffer_push_string(buffer, this.mime_type);

        buffer_push_int64(buffer, this.size);
        buffer_push_data(buffer, decode64(this.file_id), true);


        buffer_write_number(buffer, this.file_blocks.length);
        for(var i = 0; i < this.file_blocks.length; ++i){
            this.file_blocks[i].serialize(buffer);
        }
    }

    static deserialize(data) {
        const name = buffer_get_string(data);
        const mime_type = buffer_get_string(data);
        const size = buffer_get_int64(data);
        const file_id = base64(buffer_pop_data(data));

        const row_count = buffer_read_number(data);
        var file_blocks = [];
        for(var i = 0; i < row_count; ++i){
            file_blocks.push(file_block.deserialize(data));
        }

        return new file_info(name, mime_type, size, file_id, file_blocks);
    }
}


export class user_message_transaction {
    constructor(message, files)
    {
        this.message = message;
        this.files = files;
    }

    serialize() {
        let buffer = create_buffer();
        buffer.putByte(97);
        buffer_push_string(buffer, this.message);
        buffer_write_number(buffer, this.files.length);
        for(var i = 0; i < this.files.length; ++i){
            this.files[i].serialize(buffer);
        }
        return buffer;
    }


    static deserialize(data) {
        const message = buffer_get_string(data);
        const row_count = buffer_read_number(data);
        var files = [];
        for(var i = 0; i < row_count; ++i){
            files.push(file_info.deserialize(data));
        }

        return new user_message_transaction(message, files);
    }
}

export default user_message_transaction;