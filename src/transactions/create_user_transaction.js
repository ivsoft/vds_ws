import {
    buffer_push_string,
    create_buffer,
    user_credentials_to_key,
    public_key_to_Der,
    crypt_private_key,
    rsa_generateKeyPair,
    buffer_push_data
} from '../vds_crypto';

export class create_user_transaction {
    constructor(user_credentials_key, user_public_key, user_private_key, user_name){
        this.user_credentials_key = user_credentials_key;
        this.user_public_key = user_public_key;
        this.user_private_key = user_private_key;
        this.user_name = user_name;
    }

    serialize() {
        let buffer = create_buffer();
        buffer.putByte(117);
        buffer_push_string(buffer, this.user_credentials_key);
        buffer_push_data(buffer, this.user_public_key, true);
        buffer_push_data(buffer, this.user_private_key, true);
        buffer_push_string(buffer, this.user_name);
        return buffer.getBytes();
    }

    static async new_user(user_name, user_password) {
        const pair = await rsa_generateKeyPair(4096);
        return new create_user_transaction(
            user_credentials_to_key(user_password),
            public_key_to_Der(pair.publicKey),
            crypt_private_key(pair.privateKey, user_password),
            user_name
        );
    }
}