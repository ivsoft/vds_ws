import {
    buffer_push_string,
    create_buffer,
    public_key_to_Der,
    private_key_to_Der,
    rsa_generateKeyPair,
    buffer_push_data,
    buffer_pop_data,
    buffer_get_string,
    public_key_fingerprint
} from '../vds_crypto';

export class channel_create_transaction {
    constructor(channel_id, channel_type, name, read_public_key, read_private_key, write_public_key, write_private_key){
        this.channel_id = channel_id;
        this.channel_type = channel_type;
        this.channel_name = name;
        this.read_public_key = read_public_key;
        this.read_private_key = read_private_key;
        this.write_public_key = write_public_key;
        this.write_private_key = write_private_key;
    }

    serialize() {
        let buffer = create_buffer();
        
        buffer.putByte(110);

        buffer_push_data(buffer, this.channel_id, true);
        buffer_push_string(buffer, this.channel_type);
        buffer_push_string(buffer, this.channel_name);
        buffer_push_data(buffer, this.read_public_key, true);
        buffer_push_data(buffer, this.read_private_key, true);
        buffer_push_data(buffer, this.write_public_key, true);
        buffer_push_data(buffer, this.write_private_key, true);

        return buffer.getBytes();
    }

    static deserialize(data){
        const channel_id = buffer_pop_data(data);
        const channel_type = buffer_get_string(data);
        const channel_name = buffer_get_string(data);
        const read_public_key = buffer_pop_data(data);
        const read_private_key = buffer_pop_data(data);
        const write_public_key = buffer_pop_data(data);
        const write_private_key = buffer_pop_data(data);

        return new channel_create_transaction(
            channel_id,
            channel_type,
            channel_name,
            read_public_key,
            read_private_key,
            write_public_key,
            write_private_key
        );
    }

    static async channel_create(channel_type, channel_name) {
        console.log('channel_create', channel_type, channel_name);
        const read_pair = await rsa_generateKeyPair(4096);
        const write_pair = await rsa_generateKeyPair(4096);

        const channel_id = public_key_fingerprint(write_pair.publicKey);

        return new channel_create_transaction(
            channel_id,
            channel_type,
            channel_name,
            public_key_to_Der(read_pair.publicKey),
            private_key_to_Der(read_pair.privateKey),
            public_key_to_Der(write_pair.publicKey),
            private_key_to_Der(write_pair.privateKey)
        );
    }
}